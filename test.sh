#!/bin/sh

ln -sfT "$(nix-build . --no-out-link)/bin/frankenwm" "${XDG_DATA_HOME:-$HOME/.local/share}/bin/frankenwm"
