{
  pkgs ? import <nixpkgs> {},
  patches ? [],
}:

pkgs.stdenv.mkDerivation {
  pname = "FrankenWM";
  version = "git";

  src = ./.;

  buildInputs = with pkgs.xorg; [ libX11 libxcb xcbutil xcbutilkeysyms xcbutilwm ];
  nativeBuildInputs = with pkgs; [ pkg-config ];

  # Allow users set their own list of patches
  inherit patches;

  installPhase = "make DESTDIR=$out PREFIX=/ install";

  meta = {
    homepage = "https://bbs.archlinux.org/viewtopic.php?id=189060";
    description = "Dynamic window manager for X";
    license = pkgs.stdenv.lib.licenses.mit;
    maintainers = with pkgs.stdenv.lib.maintainers; [ jD91mZM2 ];
    platforms = with pkgs.stdenv.lib.platforms; all;
  };
}
